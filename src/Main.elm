module Main exposing (..)

import Browser
import Html exposing (Html, button, div, text)
import Html.Events exposing (onClick)

main =
  Browser.element { init = init, update = update, view = view, subscriptions = subscriptions }

type Msg = Increment | Decrement

init : () -> (Int, Cmd Msg)
init _ = (0, Cmd.none)

update : Msg -> Int -> (Int, Cmd Msg)
update msg model =
  case msg of
    Increment ->
      (model + 1, Cmd.none)

    Decrement ->
      (model - 1, Cmd.none)

view : Int -> Html Msg
view model =
  div []
    [ button [ onClick Decrement ] [ text "-" ]
    , div [] [ text (String.fromInt model) ]
    , button [ onClick Increment ] [ text "+" ]
    ]

subscriptions : Int -> Sub Msg
subscriptions model = Sub.none
